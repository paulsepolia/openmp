#!/bin/bash

# 1. compiling

  icpc -O3               \
       -Wall             \
       -static           \
       -openmp           \
       -openmp-report2   \
       driver_openmp.cpp \
       -o x_intel
