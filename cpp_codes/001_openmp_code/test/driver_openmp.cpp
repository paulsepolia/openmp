//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/30              //
//===============================//

#include <ctime>
#include <iostream>
#include <omp.h>

using namespace std;

// 1. function definition

int fib(int n)
{
  int i;
  int j;

  if(n < 2)
  { return n; }
  else
  {
    #pragma omp task \
     default(none)   \
     shared(i)       \
     firstprivate(n)
     {
       i = fib(n-1);
     }
    #pragma omp task \
     default(none)   \
     shared(j)       \
     firstprivate(n)
     {
       j = fib(n-2);
     }
    #pragma omp taskwait
    {
      return (i+j);
    }
  }
}

// 2. the main function

int main()
{
  // 1. parameters

  const int n        = 44;
  const int dyn_flag = 0;

  // 2. local variables

  int nt;
  double t_start_omp;
  double t_end_omp;
  time_t t_start_gnu;
  time_t t_end_gnu; 

  // 3. setting OpenMP environment

  nt = 0;

#ifdef _OPENMP
  #pragma omp parallel \
   default(none)       \
   shared(nt)
   { nt = omp_get_num_threads();
     omp_set_dynamic(dyn_flag); }
#endif

  // 3. main code execution

#ifdef _OPENMP
  t_start_omp = omp_get_wtime();
#endif
  t_start_gnu = time(NULL);

  cout << endl;

  #pragma omp parallel \
   default(none)       \
   shared(std::cout)
   {
     #pragma omp single
     cout <<  " 1 --> fib(" << n << ") = " << fib(n) << endl;
   }

#ifdef _OPENMP
  t_end_omp = omp_get_wtime();
#endif
  t_end_gnu = time(NULL);

  // 4. the outputs

#ifdef _OPENMP
  cout << " 2 --> Real time used is (openmp) : " << (t_end_omp-t_start_omp) << endl;
#endif
  cout << " 3 --> Real time used is (gnu)    : " << (t_end_gnu-t_start_gnu) << endl; 
  cout << " 4 --> Number of threads are      : " << nt << endl;
  cout << endl;

  return 0;
}

//======//
// FINI //
//======//
