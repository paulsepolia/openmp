//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/09/16              //
//===============================//

#include <iostream>
#include <omp.h>

using namespace std;

// 1. the "subdomain" function

void subdomain(double *x, long istart, long ipoints)
{
  long i;
  long j;
  const long J_MAX = 100;

  for(i = 0; i < ipoints; i++)
  { 
    for (j = 0; j < J_MAX; j++)
    { x[istart+i] = static_cast<double>(i+j); }
  }

}

// 2. the "sub" function

void sub(double *x, long npoints)
{
  long iam;
  long nt;
  long ipoints;
  long istart;

  #pragma omp parallel \
   default(none)       \
   private(iam)        \
   private(nt)         \
   private(ipoints)    \
   private(istart)     \
   shared(npoints)     \
   shared(x)
   {
     iam = omp_get_thread_num();
     nt = omp_get_num_threads();
     ipoints = npoints / nt;     // size of partition
     istart = iam * ipoints;     // starting array index
     if (iam == nt-1)            // last thread may do more
     { ipoints = npoints - istart; }
  
     subdomain(x, istart, ipoints);
   }
}

// 3. the main function

int main()
{
  // 4. local variables
  
  const long DIM_ARRAY = 100000000;

  double *array_loc = new double [DIM_ARRAY];
  
  // 5. main program

  sub(array_loc, DIM_ARRAY);

  cout << endl;
  cout << " array_loc[0] = " << array_loc[0] << endl;
  cout << " array_loc[1] = " << array_loc[1] << endl;
  cout << " array_loc[2] = " << array_loc[2] << endl;
  cout << " array_loc[3] = " << array_loc[3] << endl;
  cout << " array_loc[4] = " << array_loc[4] << endl;
  cout << endl; 

  return 0;
}

//======//
// FINI //
//======//
