//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/09/11              //
//===============================//

#include <iostream>
#include <omp.h>

using namespace std;

// 1. the main function

int main()
{
  // 2. local variables

  int flag = 0;
  int i_loc;

  // 3. parallel region with a race condition

  #pragma omp parallel \
   default(none)       \
   num_threads(3)      \
   shared(flag)        \
   private(i_loc)      \
   shared(std::cout)
  {
    if (omp_get_thread_num() == 0)
    {
      // set flag to release thread 1
      #pragma omp atomic
       flag++;
      // flush of flag is implied by the atomic directive
    }
    else if (omp_get_thread_num() == 1)
    {
      // loop until we see the flag reaches 1
      i_loc = 0;
      while(flag < 1)
      {
        i_loc = i_loc + 1;
        #pragma omp flush(flag)
      }
      cout << " Thread 1 awoken " << endl;
      cout << " Inside Thread 1, loop counter is " << i_loc << endl;

      // set flag to release thread 2
      #pragma omp atomic
       flag++;
      // flush of flag is implied by the atomic directive 
    }
    else if (omp_get_thread_num() == 2)
    {
      // loop until we see the flag reaches 2
      i_loc = 0;
      while(flag < 2)
      {
        i_loc = i_loc + 1;
        #pragma omp flush(flag)
      }
      cout << " Thread 2 awoken " << endl;
      cout << " Inside Thread 2, loop counter is " << i_loc << endl;
    }
  }
 
  return 0;
}

//======//
// FINI //
//======//
