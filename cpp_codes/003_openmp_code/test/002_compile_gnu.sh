#!/bin/bash

# 1. compiling

  g++ -O3               \
      -Wall             \
      -fopenmp          \
      -static           \
      driver_openmp.cpp \
      -o x_gnu
