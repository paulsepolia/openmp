//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/09/11              //
//===============================//

#include <ctime>
#include <iostream>
#include <omp.h>

using namespace std;

// 1. the main function

int main()
{
  // 2. local variables

  int data;
  int flag = 0;
  
  // 3. parallel region with a race condition

  #pragma omp parallel \
   default(none)       \
   num_threads(4)      \
   shared(data)        \
   shared(flag)        \
   shared(std::cout)
  {
    if (omp_get_thread_num() == 0)
    {
      // write to the data buffer that will be read by the thread
      data = 42;
      // flush data to thread 1 and strictly order
      // the write to data relative to the write to the flag
      #pragma omp flush(flag, data)
      // set flag to release thread 1
      flag = 1;
      // flush flag to ensure that thread 1 sees the change
      #pragma omp flush(flag)
    }
    else if (omp_get_thread_num() == 1)
    {
      // loop until we see the update to the flag
      #pragma omp flush(flag, data)
      while (flag < 1)
      { 
        #pragma omp flush(flag, data)
      }
      // values of flag and data are undefined
      cout << " --> 1 --> flag = " << flag << " --> 2 --> data = " << data << endl;
      // values of data will be 42, value of flag still undefined
      cout << " --> 3 --> flag = " << flag << " --> 4 --> data = " << data << endl;
    }
  }
 
  return 0;
}

//======//
// FINI //
//======//
