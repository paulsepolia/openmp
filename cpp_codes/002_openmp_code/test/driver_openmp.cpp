//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/09/11              //
//===============================//

#include <ctime>
#include <iostream>
#include <omp.h>

using namespace std;

// 1. the main function

int main()
{
  // 2. local variables

  int x;

  // 3. global value

  x = 2;
  
  // 4. parallel region with a race condition

  #pragma omp parallel \
   default(none)       \
   num_threads(4)      \
   shared(x)           \
   shared(std::cout)
  {
    if (omp_get_thread_num() == 0)
    { x = 5; }
    else 
    { // the following cout has a race
      cout << " 1 --> Thread " << omp_get_thread_num() << " has value x " << x << endl;
    }
 
    #pragma omp barrier

    if (omp_get_thread_num() == 0)
    {
      cout << " 2 --> Thread " << omp_get_thread_num() << " has value x " << x << endl;
    }
    else
    {
      cout << " 3 --> Thread " << omp_get_thread_num() << " has value x " << x << endl;
    }

 }
 
  return 0;
}

//======//
// FINI //
//======//
