//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/09/16              //
//===============================//

#include <iostream>
#include <omp.h>

using namespace std;

int main()
{
  omp_set_nested(1);
  omp_set_dynamic(0);
  #pragma omp parallel
  {
    #pragma omp parallel
    {
      #pragma omp single
      {
        // if OMP_NUM_THREADS = 3 was set, the following should print:
        // Inner: num_threads = 3
        // if nesting is not supported, the following should print:
        // Inner: num_threads = 1
  
        cout << " Inner: num_threads = " << omp_get_num_threads() << endl;
      }
    }

    #pragma omp barrier
    omp_set_nested(0);
    #pragma omp parallel
    {
      #pragma omp single
      {
        // Even if OMP_NUM_THREADS = 3 was set, the following should print,
        // because nesting is disabled:
        // Inner: num_threads = 1
        // Inner: num_threads = 1

        cout << " Inner: num_threads = " << omp_get_num_threads() << endl;
      }
    }
   
    #pragma omp barrier
    #pragma omp single
    {
      // If OMP_NUM_THREAD = 2 was set, the following should print:
      // Outer: num_threads = 2

      cout << " Outer: num_threads = " << omp_get_num_threads() << endl;
    } 

  }
  
  return 0;
}

//======//
// FINI //
//======//
