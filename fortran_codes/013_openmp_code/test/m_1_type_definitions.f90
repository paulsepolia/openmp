!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/05              !
!===============================!

  module m_1_type_definitions

  implicit none
 
!==================================================!
! 1. Common type definitions                       !         
! a. Here I define all the common type definitions !
! b. These kind types are used in any module       !
!==================================================!

  integer, parameter :: dr = selected_real_kind(p=15,r=300)
  integer, parameter :: sr = selected_real_kind(p=6,r=30)
  integer, parameter :: di = selected_int_kind(18)
  integer, parameter :: si = selected_int_kind(9)
 
  end module m_1_type_definitions

!======!
! FINI !
!======!
