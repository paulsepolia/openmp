!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/16              !
!===============================!

  program driver_openmp

  use m_1_type_definitions
  use omp_lib

  implicit none

  !  1. local parameters

  integer(kind=si) :: j
  integer(kind=si) :: k
  integer(kind=si) :: jlast
  integer(kind=si) :: klast
 
  !  2. main parallel openmp code

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(jlast) &
  !$omp shared(klast) 

  !$omp do                 &
  !$omp private(j)         &
  !$omp private(k)         &
  !$omp lastprivate(jlast) &
  !$omp lastprivate(klast)

  do k = 1, 2
    do j = 1, 3

      jlast = j
      klast = k
  
    end do
  end do

  !$omp end do

  !$omp single

  write(*,*) " klast = ", klast
  write(*,*) " jlast = ", jlast

  !$omp end single
  
  !$omp end parallel
 
  end program driver_openmp

!======!
! FINI !
!======!
