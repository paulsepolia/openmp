!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/28              !
!===============================!

  program driver_openmp

  use m_1_type_definitions
  use omp_lib

  implicit none

  real(kind=dr) :: a
  real(kind=dr) :: b
  real(kind=dr) :: sum_loc
  real(kind=dr) :: h
  integer(kind=si) :: i
  integer(kind=si) :: n

  a = 2.0_dr
  b = 10.0_dr
  n = 100000_si
  h = (b - a)/real(n,kind=dr)
  sum_loc = 0.0_dr
  
  !$omp parallel      &
  !$omp default(none) &
  !$omp private(i)    &
  !$omp shared(a)     &
  !$omp shared(h)     &
  !$omp shared(n)     &
  !$omp reduction(+:sum_loc)
  !$omp do

  do i = 1,n-1
    sum_loc = sum_loc + f(a+real(i,kind=dr)*h)
  end do

  !$omp end do
  !$OMP end parallel
  
  sum_loc = h*(sum_loc + (f(a)+ f(b))/2.0_dr)

  write(*,*) "Integral is --> ", sum_loc

  contains

  function f(x)
  
  use m_1_type_definitions

  implicit none
      
  real(kind=dr) :: f
  real(kind=dr) :: x

  f = x*x

  end function f

  end program driver_openmp

!======!
! FINI !
!======!
