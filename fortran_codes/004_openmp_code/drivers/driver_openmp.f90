!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/29              !
!===============================!

  program driver_openmp

#ifdef _OPENMP
  use omp_lib
#endif

  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=si), parameter :: K_DO_MAX = 10_si
  integer(kind=si), parameter :: DIMEN    = 10000_si 

  !  2. local variables  

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: k
  integer(kind=si) :: count_sys_a
  integer(kind=si) :: count_sys_b
  integer(kind=si) :: count_rate
  integer(kind=si) :: count_max
  integer(kind=si) :: nt
  integer(kind=si) :: phys_cores
  real(kind=dr), allocatable, dimension(:,:) :: a_matrix
  real(kind=dr), allocatable, dimension(:,:) :: b_matrix
  real(kind=dr), allocatable, dimension(:,:) :: c_matrix
 
  !  3. allocate RAM for the matrices

  allocate(a_matrix(1:DIMEN, 1:DIMEN))
  allocate(b_matrix(1:DIMEN, 1:DIMEN))
  allocate(c_matrix(1:DIMEN, 1:DIMEN)) 
  
  !  4. build the matrices in parallel for K_DO_MAX times

  write(*,*)
  write(*,*) "  1 --> Parallel region starts"
  write(*,*) "  2 --> Build the 3 matrices in parallel"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX  ! benchmark starts here

    !$omp parallel do      &
    !$omp default(none)    &
    !$omp private(i,j)     &
    !$omp shared(a_matrix) &
    !$omp shared(b_matrix) &
    !$omp shared(c_matrix)
 
    do i = 1, DIMEN
      do j = 1, DIMEN

        a_matrix(j,i) = 1.0_dr
        b_matrix(j,i) = 2.0_dr
        c_matrix(j,i) = 3.0_dr
    
      end do
    end do

    !$omp end parallel do

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  3 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  5. Use of WORKSHARE Fortran90 OpenMP construct

  write(*,*) "  4 --> Parallel region starts"
  write(*,*) "  5 --> WORKSHARE OpenMP construct in use"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX

    !$omp parallel         &
    !$omp default(none)    &
    !$omp shared(a_matrix) &
    !$omp shared(b_matrix) &
    !$omp shared(c_matrix)
    !$omp workshare

    b_matrix(1:DIMEN,1:DIMEN) = b_matrix(1:DIMEN,1:DIMEN) + 1.0_dr
    c_matrix(1:DIMEN,1:DIMEN) = c_matrix(1:DIMEN,1:DIMEN) + 2.0_dr
    a_matrix(1:DIMEN,1:DIMEN) = b_matrix(1:DIMEN,1:DIMEN) + c_matrix(1:DIMEN,1:DIMEN)

    !$omp end workshare
    !$omp end parallel

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  6 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  6. final OpenMP step

  write(*,*) "  7 --> Loop steps are: ", K_DO_MAX
  write(*,*) "  8 --> Size of the matrices is: ", DIMEN, " x ", DIMEN

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)    &
  !$omp shared(phys_cores)
  
#ifdef _OPENMP
  nt = omp_get_num_threads()
  phys_cores = omp_get_num_procs()
#else
  nt = 1_si
  phys_cores = 1_si
#endif

  !$omp end parallel

  write(*,*) "  9 --> Number of OpenMP threads = ", nt
  write(*,*) " 10 --> Number of physical cores = ", phys_cores
  write(*,*) " 11 --> Exit"  

  !  7. deallocate the RAM

  deallocate(a_matrix)
  deallocate(b_matrix)
  deallocate(c_matrix)

  end program driver_openmp

!======!
! FINI !
!======!
