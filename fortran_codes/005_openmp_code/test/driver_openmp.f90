!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/30              !
!===============================!

  program driver_openmp

#ifdef _OPENMP
  use omp_lib
#endif

  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=si), parameter :: K_DO_MAX = 100_si
  integer(kind=di), parameter :: DIMEN    = 4_di * 100000000_di

  !  2. local variables  

  integer(kind=di) :: i
  integer(kind=si) :: k
  integer(kind=si) :: iam
  integer(kind=di) :: low_i
  integer(kind=di) :: up_i
  integer(kind=si) :: count_sys_a
  integer(kind=si) :: count_sys_b
  integer(kind=si) :: count_rate
  integer(kind=si) :: count_max
  integer(kind=si) :: nt
  integer(kind=si) :: phys_cores
  real(kind=dr), allocatable, dimension(:) :: a
  real(kind=dr), allocatable, dimension(:) :: b
 
  !  3. allocate RAM for the matrices

  allocate(a(1:DIMEN))
  allocate(b(1:DIMEN))
  
  !  4. build the matrices in parallel for K_DO_MAX times

  write(*,*)
  write(*,*) "  1 --> Parallel region starts"
  write(*,*) "  2 --> Build the 2 matrices using parallel do"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX  ! benchmark starts here

    !$omp parallel do   &
    !$omp default(none) &
    !$omp private(i)    &
    !$omp shared(a)     &
    !$omp shared(b)

    do i = 1, DIMEN

      a(i) = 1.0_dr
      b(i) = 2.0_dr

    end do

    !$omp end parallel do

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  3 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  5. manipulate the matrices in parallel for K_DO_MAX times
  !     using the OpenMP parallel do construct

  write(*,*) "  4 --> Parallel region starts"
  write(*,*) "  5 --> Matrix manipulation using parallel do"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX  ! benchmark starts here

    !$omp parallel do   &
    !$omp default(none) &
    !$omp private(i)    &
    !$omp shared(a)     &
    !$omp shared(b)
 
    do i = 1, DIMEN

        a(i) = a(i) + 0.25_dr * b(i)
 
    end do

    !$omp end parallel do

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  6 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  6. manipulate the matrices in parallel for K_DO_MAX times
  !     using the OpenMP parallel construct 

  write(*,*) "  7 --> Parallel region starts"
  write(*,*) "  8 --> Matrix manipulation using parallel"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX

    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)     &
    !$omp shared(a)      &
    !$omp shared(b)      &
    !$omp private(i)     &
    !$omp private(iam)   &
    !$omp private(low_i) &
    !$omp private(up_i)

    !$omp single
#ifdef _OPENMP
      nt  = omp_get_num_threads()
#else
      nt = 1_si
#endif
    !$omp end single

#ifdef _OPENMP
      iam   = omp_get_thread_num()
#else
      iam = 0_si
#endif
      low_i = DIMEN * iam/nt + 1_si
      up_i  = DIMEN * (iam+1)/nt
 
    !$omp do
      do i = low_i, up_i

        a(i) = a(i) + 0.25_dr * b(i)

      end do
    !$omp end do

    !$omp end parallel

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  9 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  7. building again the matrices in parallel for K_DO_MAX times
  !     using the OpenMP parallel construct 

  write(*,*) " 10 --> Parallel region starts"
  write(*,*) " 11 --> Build the 2 matrices using parallel"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX

    !$omp parallel       &
    !$omp default(none)  &
    !$omp shared(nt)     &
    !$omp shared(a)      &
    !$omp shared(b)      &
    !$omp private(i)     &
    !$omp private(iam)   &
    !$omp private(low_i) &
    !$omp private(up_i)

    !$omp single
#ifdef _OPENMP
      nt  = omp_get_num_threads()
#else
      nt = 1
#endif
    !$omp end single

#ifdef _OPENMP
      iam   = omp_get_thread_num()
#else
      iam = 0_si
#endif
      low_i = DIMEN * iam/nt + 1_si
      up_i  = DIMEN * (iam+1)/nt
 
    !$omp do
      do i = low_i, up_i

        a(i) = 1.0_dr
        b(i) = 2.0_dr

      end do
    !$omp end do

    !$omp end parallel

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) " 12 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"
  !  8. final OpenMP step

  write(*,*) " 13 --> Loop steps are: ", K_DO_MAX
  write(*,*) " 14 --> Size of vectors is: ", DIMEN

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)    &
  !$omp shared(phys_cores)
  
#ifdef _OPENMP
  nt = omp_get_num_threads()
  phys_cores = omp_get_num_procs()
#else
  nt = 1_si
  phys_cores = 1_si
#endif

  !$omp end parallel

  write(*,*) " 15 --> Number of OpenMP threads = ", nt
  write(*,*) " 16 --> Number of physical cores = ", phys_cores
  write(*,*) " 17 --> Exit"  

  !  9. deallocate the RAM

  deallocate(a)
  deallocate(b)

  end program driver_openmp

!======!
! FINI !
!======!
