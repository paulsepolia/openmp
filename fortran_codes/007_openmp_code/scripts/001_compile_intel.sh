#!/bin/bash

# 1. compiling

  ifort -O3                      \
        -warn all                \
        -static                  \
        -openmp                  \
        -openmp-report2          \
        -cpp                     \
        m_1_type_definitions.f90 \
        driver_openmp.f90        \
        -o x_intel

# 2. cleaning 

  rm *.mod
