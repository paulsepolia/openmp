!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/30              !
!===============================!

  program driver_openmp

#ifdef _OPENMP
  use omp_lib
#endif

  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=si), parameter :: K_DO_MAX = 100_si
  integer(kind=di), parameter :: DIMEN    = 6_di * 10000000_di

  !  2. local variables  

  integer(kind=di) :: i
  integer(kind=di) :: k
  integer(kind=si) :: count_sys_a
  integer(kind=si) :: count_sys_b
  integer(kind=si) :: count_rate
  integer(kind=si) :: count_max
  integer(kind=si) :: nt
  integer(kind=si) :: phys_cores
  real(kind=dr), save :: sum_loc_a
  real(kind=dr), save :: sum_loc_b
  real(kind=dr), save :: sum_loc_c
  real(kind=dr), save :: sum_loc_d
  !$omp threadprivate(sum_loc_a) 
  !$omp threadprivate(sum_loc_b) 
  !$omp threadprivate(sum_loc_c) 
  !$omp threadprivate(sum_loc_d)
 
 
  !  3. build the matrices in parallel for K_DO_MAX times

  write(*,*)
  write(*,*) "  1 --> Parallel region starts"
  write(*,*) "  2 --> Opening many tasks"

  call system_clock(count_sys_a, count_rate, count_max)


  sum_loc_a = 0.0_dr
  sum_loc_b = 0.0_dr
  sum_loc_c = 0.0_dr
  sum_loc_d = 0.0_dr

  do k = 1, K_DO_MAX  ! benchmark starts here

    !$omp parallel      &
    !$omp default(none) &
    !$omp private(i)

    !$omp task 
    do i = 1, DIMEN
      sum_loc_a = sum_loc_a + real(i,kind=dr)
    end do
    !$omp end task

    !$omp task
    do i = 1, DIMEN
      sum_loc_b = sum_loc_b + real(i,kind=dr)
    end do
    !$omp end task

    !$omp task
    do i = 1, DIMEN
      sum_loc_c = sum_loc_c + real(i,kind=dr)
    end do
    !$omp end task

    !$omp task
    do i = 1, DIMEN
      sum_loc_d = sum_loc_d + real(i,kind=dr)
    end do
    !$omp end task

    !$omp end parallel

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  3 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  4. final OpenMP step

  write(*,*) "  4 --> Loop steps are: ", K_DO_MAX
  write(*,*) "  5 --> Tasks opened: 4"

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)    &
  !$omp shared(phys_cores)
  
#ifdef _OPENMP
  nt = omp_get_num_threads()
  phys_cores = omp_get_num_procs()
#else
  nt = 1_si
  phys_cores = 1_si
#endif

  !$omp end parallel

  write(*,*) "  6 --> Number of OpenMP threads = ", nt
  write(*,*) "  7 --> Number of physical cores = ", phys_cores
  write(*,*) "  8 --> sum_loc_a = ", sum_loc_a
  write(*,*) "  9 --> sum_loc_b = ", sum_loc_b
  write(*,*) " 10 --> sum_loc_c = ", sum_loc_c
  write(*,*) " 11 --> sum_loc_d = ", sum_loc_d
  write(*,*) " 12 --> Exit"

  end program driver_openmp

!======!
! FINI !
!======!
