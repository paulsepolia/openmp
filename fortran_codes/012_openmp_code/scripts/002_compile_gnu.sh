#!/bin/bash

# 1. compiling

  gfortran -O3                      \
           -Wall                    \
           -std=f2008               \
           -fopenmp                 \
           -static                  \
           -cpp                     \
           m_1_type_definitions.f90 \
           m_2_cholesky_subs.f90    \
           driver_openmp.f90        \
           -o x_gnu

# 2. cleaning 

  rm *.mod
