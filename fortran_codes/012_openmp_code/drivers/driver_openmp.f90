!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/05              !
!===============================!

  program driver_openmp

  use m_1_type_definitions
  use m_2_cholesky_subs
  use omp_lib

  implicit none

  !  1. local parameters

  integer(kind=si), parameter :: N = 4000_si

  !  2. local variables

  real(kind=dr), dimension(:,:), allocatable :: A
  real(kind=dr), dimension(:,:), allocatable :: copy_A
  real(kind=dr) :: tstart
  real(kind=dr) :: tend
  
  !  3. program starts - timing starts

  allocate(A(1:N,1:N))
  allocate(copy_A(1:N,1:N))

  tstart = omp_get_wtime()

  !  4. generate a positive definite matrix

  call gen_posdef(A)

  !  5. take copy of input data, as we are going to overwrite it

  copy_A = A
  
  !  6. call factorization routine

  call cholesky(A) 
  
  !  7. check the factorization

  call check_fact(A, copy_A)

  !  8. timing ends

  tend = omp_get_wtime()
  
  write(*,*) "  1 --> Time taken = ", tend-tstart
  
  end program driver_openmp

!======!
! FINI !
!======!
