!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/05              !
!===============================!

  module m_2_cholesky_subs

  use m_1_type_definitions
  use omp_lib

  implicit none

  contains 

  !===========================!
  ! A. subroutine: dotproduct !
  !===========================!
 
  subroutine dotproduct(x, y, dotprod)

  implicit none

  !  1. interface variables

  real(kind=dr), dimension(:), intent(in) :: x
  real(kind=dr), dimension(:), intent(in) :: y
  real(kind=dr), intent(inout) :: dotprod

  !  2. local variables

  integer(kind=si) :: i

  !  3. test

  if (size(x) /= size(y)) then
    stop "Arrays passed to dot product routine must be the same size."
  end if

  !  4. the work can be shared equally over the threads

  !$omp do                   &
  !$omp private(i)           &
  !$omp schedule(static)     &
  !$omp reduction(+:dotprod) 

  do i = 1, size(x)
    dotprod = dotprod + (x(i)*y(i))
  end do

  !$omp end do

  end subroutine dotproduct
 
  !========================!
  ! B. subroutine: MAT_MUL !
  !========================!
 
  subroutine mat_mul(A, B, C, Ajj)
    
  implicit none

  !  1. interface variables

  real(kind=dr), dimension(:),   intent(inout) :: A
  real(kind=dr), dimension(:,:), intent(in)    :: B
  real(kind=dr), dimension(:),   intent(in)    :: C
  real(kind=dr), intent(in)                    :: Ajj

  !  2. local variables

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: m
  integer(kind=si) :: n

  !  3. test

  m = size(B, dim=1)
  n = size(C)

  if (size(B, dim=2) /= n) then
    stop "Dimensions of matrices passed to matrix multiply not suitable."
  end if

  !  4. the work can be shared equally over the threads

  !$omp do               &
  !$omp schedule(static) &
  !$omp private(i)       &
  !$omp private(j)

  do i = 1, m 
    do j = 1, n
      A(i) = A(i) - (B(i,j) * C(j))
    end do
    ! scale the column
    A(i) = A(i)/Ajj
  end do

  !$omp end do

  end subroutine mat_mul

  !=========================!
  ! C. subroutine: cholesky !
  !=========================!

  subroutine cholesky(A)
  
  ! Compute the factor L in A=LL^T, where
  ! A is symmetric positive definite and
  ! L is lower triangular
  ! We overwrite the lower triangle of A with L
 
  implicit none
 
  !  1. local variables

  integer(kind=si) :: j
  integer(kind=si) :: n
  integer(kind=si) :: iam
  real(kind=dr) :: Ajj
  real(kind=dr) :: dotprod

  !  2. interface variables

  real(kind=dr), dimension(:,:), intent(inout) :: A

  !  3. main code

  n = size(A, 1)

  !$omp parallel        &
  !$omp default(none)   &
  !$omp shared(n)       &
  !$omp shared(A)       &
  !$omp shared(dotprod) &
  !$omp private(j)      &
  !$omp private(Ajj)    &
  !$omp private(iam)

  iam = omp_get_thread_num()
  Ajj = 0.0_dr

  ! Loop over columns of A
  ! Store the nonzero elements of L in lower triangle of A
    
  DO j = 1, n
    ! Compute: Ajj = A(j,j) - dot product of A(j,1:j-1) with itself
    dotprod = 0.0_dr

    !$omp barrier
    call dotproduct(A(j,1:j-1), A(j,1:j-1), dotprod)  
    Ajj = A(j,j) - dotprod

    !$omp barrier
    if (Ajj <= 0.0_dr) then
      ! A is not positive definite
      stop "Matrix generated not positive definite"
    end if

    Ajj = sqrt(Ajj) 

    !$omp single
    A(j,j) = Ajj
    !$omp end single
       
    ! Compute elements j+1:n of column j.
    if (j < n ) then
      ! Compute A(j+1:n,j) =  A(j+1:n,j) - M, where
      ! M is the matrix multiplication of A(j+1:n,1:j-1) with A(j,1:j-1)
      call mat_mul(A(j+1:n,j),A(j+1:n,1:j-1), A(j,1:j-1),Ajj )
    end if
  end do
  !$omp end parallel

  end subroutine cholesky

  !============================!
  !  D. Subroutine: gen_posdef !
  !============================!

  subroutine gen_posdef(A)

  ! Generate a random positive definite symmetric matrix
    
  implicit none

  !  1. local variables
 
  integer(kind=si) :: i
  integer(kind=si) :: n
  integer(kind=si) :: m
  real(kind=dr), dimension(:,:), allocatable :: B
  real(kind=dr), dimension(:,:), allocatable :: C

  !  2. interface variables

  real(kind=dr), dimension(:,:), intent(inout), allocatable :: A

  !  3. main code

  n = size(A, 1)
  m = size(A, 2)
  allocate(B(1:n, 1:m))
  allocate(C(1:n, 1:m))
 
  !  4. Assume this gives full rank matrix 
  
  call random_number(A) 

  !  5. parallel OpenMP here

  ! Make sure A is diagonal dominant  

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(n)     &
  !$omp shared(m)     &
  !$omp shared(A)     &
  !$omp shared(B)     &
  !$omp shared(C)     &
  !$omp private(i) 

  !$omp workshare
  A(1:n,1:m) = A(1:n,1:m) - 0.5_dr
  !$omp end workshare

  !$omp do               &
  !$omp schedule(static) &
  !$omp private(i)
  do i = 1, n
    A(i, i) = A(i, i) + 10.0_dr
  end do
  !$omp end do

  !$omp master
  A(1:n,1:m) = A(1:n,1:m) / 10_dr
  B(1:n,1:m) = TRANSPOSE(A(1:n,1:m))
  C(1:n,1:m) = MATMUL(B(1:n,1:m), A(1:n,1:m))
  A(1:n,1:m) = C(1:n,1:m)
  !$omp end master

  !$omp end parallel

  !  6. free up RAM

  deallocate(B)
  deallocate(C)

  end subroutine gen_posdef
 
  !===========================!
  ! E. Subroutine: check_fact !
  !===========================!
 
  subroutine check_fact(L, A)
  ! Check the sum of the elements of A-LL^T
  ! scaled by n, should be approximately zero
 
  implicit none
  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: n
  integer(kind=si) :: m
  real(kind=dr) :: sum_ele
  real(kind=dr), dimension(:,:), intent(in),    allocatable :: A
  real(kind=dr), dimension(:,:), intent(inout), allocatable :: L    
  real(kind=dr), dimension(:,:), allocatable :: B
  real(kind=dr), dimension(:,:), allocatable :: C

  n = size(L, 1)
  m = size(L, 2)

  allocate(B(1:n, 1:m))
  allocate(C(1:n, 1:m))

  !$omp parallel        &
  !$omp default(none)   &
  !$omp shared(L)       &
  !$omp shared(A)       &
  !$omp shared(sum_ele) &
  !$omp shared(n)       &
  !$omp shared(m)       &
  !$omp shared(B)       &
  !$omp shared(C)       &
  !$omp private(i)      &
  !$omp private(j)

  ! zero out the matrix
 
  !$omp do                   &
  !$omp schedule(static, 20)
  do i = 2, n
    do j = 1, i-1
      L(j, i) = 0.0_dr
    end do
  end do 
  !$omp end do

  !$omp workshare
  B(1:n,1:m) = transpose(L(1:n,1:m))
  !$omp end workshare
  
  !$omp master
  C(1:n,1:m) = matmul(L(1:n,1:m),B(1:n,1:m))
  !$omp end master

  !$omp workshare
  sum_ele = sum(A(1:n,1:m) - C(1:n,1:m))  
  !$omp end workshare

  !$omp end parallel 

  write(*,*) "Sum of elements is: ", sum_ele/real(n, kind=dr)

  deallocate(B)
  deallocate(C)
 
  end subroutine check_fact
  
  end module m_2_cholesky_subs

!======!
! FINI !
!======!
