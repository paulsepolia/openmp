!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/28              !
!===============================!

  program driver_openmp

#ifdef _OPENMP
  use omp_lib
#endif

  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=di), parameter :: I_DO_MAX = 20_di * int(10E2, kind=di)
  integer(kind=di), parameter :: MOD_LOCK = 100_di

  !  2. local variables

  integer(kind=di)            :: i
  integer(kind=si)            :: count_sys_a
  integer(kind=si)            :: count_sys_b
  integer(kind=si)            :: count_rate
  integer(kind=si)            :: count_max
  integer(kind=si)            :: nt
  integer(kind=si)            :: phys_cores
#ifdef _OPENMP
  integer(kind=OMP_LOCK_KIND) :: tmp_var
#else
  integer(kind=si)            :: tmp_var
#endif

  !  3. parallel region with empty do-loop and a lock/unlock in its body

  write(*,*)
  write(*,*) "  1 --> Parallel region starts"
  write(*,*) "  2 --> Empty do-loop with a lock/unlock in its body"

  call system_clock(count_sys_a, count_rate, count_max)

#ifdef _OPENMP 
  call omp_init_lock(tmp_var)
#endif

  !$omp parallel      &
  !$omp default(none) &
  !$omp private(i)    &
  !$omp shared(tmp_var)
    do i = 1, I_DO_MAX
      tmp_var = int(mod(i, MOD_LOCK) + 1_di, kind=si)
#ifdef _OPENMP 
      call omp_set_lock(tmp_var)
      call omp_unset_lock(tmp_var)
#endif
    end do
  !$omp end parallel

#ifdef _OPENMP
  call omp_destroy_lock(tmp_var)
#endif

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  3 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"  

  !  4. parallel region with an empty do-loop as its body

  call system_clock(count_sys_a, count_rate, count_max)

  write(*,*) "  4 --> Parallel region starts"
  write(*,*) "  5 --> Empty do-loop without a lock/unlock in its body"

  !$omp parallel      &
  !$omp default(none) &
  !$omp private(i)
    do i = 1, I_DO_MAX
    end do
  !$omp end parallel

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  6 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"  

  !  5. serial do-loop

  write(*,*) "  7 --> Serial empty do-loop"

  call system_clock(count_sys_a, count_rate, count_max)

  do i = 1, I_DO_MAX
  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  8 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"  

  !  6. final step

  write(*,*) "  9 --> Loop steps are: ", I_DO_MAX
  
  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)    &
  !$omp shared(phys_cores)

#ifdef _OPENMP
  nt = omp_get_num_threads()
  phys_cores = omp_get_num_procs()
#else
  nt = 1_si
  phys_cores = 1_si
#endif

  !$omp end parallel

  write(*,*) " 10 --> Number of OpenMP threads = ", nt
  write(*,*) " 11 --> Number of physical cores = ", phys_cores
  write(*,*) " 12 --> Exit"  

  end program driver_openmp

!======!
! FINI !
!======!
