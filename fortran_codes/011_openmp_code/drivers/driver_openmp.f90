!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/04              !
!===============================!

  program driver_openmp 

  use m_1_type_definitions
  use omp_lib

  implicit none

  !  1. local variables

  integer(kind=di) :: end_number
  integer(kind=di) :: i 
  integer(kind=di) :: j
  integer(kind=di) :: countprimes
  integer(kind=di) :: max_prime
  integer(kind=di) :: start
  integer(kind=di) :: start_number
  real(kind=dr)    :: tstart
  real(kind=dr)    :: tend
  logical          :: prime

  !  2. initialize the variables
  !     the start and end of the range in which to search for primes

  start = 2_di
  end_number = 100000000_di
  max_prime = 0_di

  !  3. Deal with 1 and 2 if the lower value of the range is less than 3. 
  !     remember to count 2 as a prime

  if(start <= 2_di) then

    countprimes = 1_di
    max_prime = 2_di
    start_number = 3_di

  !  4. No even number can be prime so we want to start from an odd number
  !     so we can skip checking the even numbers using a step of 2 in the DO loops

  else if(mod(start,2_di).eq.0_di) then
     countprimes = 0_di 
     start_number = start + 1_di
  else
     start_number = start
     countprimes = 0_di
  end if
  tstart = OMP_GET_WTIME()

  !  5. paralle openmp code

  !$omp parallel             &
  !$omp default(none)        &
  !$omp shared(start_number) &
  !$omp shared(end_number)   &
  !$omp shared(countprimes)  &
  !$omp shared(max_prime)    &
  !$omp private(prime)       &
  !$omp private(i)           &
  !$omp private(j)

  !$omp do &
  !$omp schedule(static,100)     &
  !$omp reduction(+:countprimes) &
  !$omp reduction(MAX:max_prime)

  do j = start_number, end_number, 2_di
  !
  ! Set a flag to indicate if we have found a prime
  ! Default to .true.
  !
    prime = .true.

    do i = 3_di, int(sqrt(real(j,kind=dr)))+1_di, 2_di
      if(mod(j,i).eq.0_di) then
        prime = .false.
        exit
      end if
    end do

    if(prime) then
    !
    ! store the current largest prime
    !
      max_prime = j
      countprimes = countprimes + 1
    end if

  end do
  !$omp end do
  !$omp end parallel

  tend = omp_get_wtime()

  write(*,*)'There are', countprimes, 'primes between', start, 'and', end_number
  write(*,*)'The largest is', max_prime
  write(*,*)'Time taken in parallel', tend-tstart
  !
  !  6. serial version to compare
  !
  if(start==2_di) THEN
    countprimes = 1_di
    start_number = start + 1_di
  else if(mod(start,2_di).eq.0)THEN
    countprimes = 0_di
    start_number = start + 1_di
  else
    start_number = start
    countprimes = 0_di
  end if

  tstart = omp_get_wtime()
 
  do j = start_number, end_number, 2_di
  !
  ! Set a flag to indicate if we have found a prime
  ! Default to .true.
  !
    prime = .true.

    do i = 3_di, int(sqrt(real(j)))+1_di, 2_di
      if(mod(j,i).eq.0_di) then
        prime = .false.
        exit
      end if
    end do

    if(prime) then
      max_prime = j
      countprimes = countprimes + 1_di
    end if

  end do

  tend = omp_get_wtime()

  ! 7. output
 
  write(*,*)'There are', countprimes, 'primes between', start, 'and', end_number
  write(*,*)'The largest is', max_prime
  write(*,*)'Time taken in serial', tend-tstart
 
  end program driver_openmp

!======!
! FINI !
!======!
