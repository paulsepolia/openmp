#!/bin/bash

#  1. Copy the drivers
 
   cp driver* ../drivers/

#  2. Copy the modules

   cp m_*  ../modules/

#  3. copy the scripts

   cp 0*   ../scripts/
