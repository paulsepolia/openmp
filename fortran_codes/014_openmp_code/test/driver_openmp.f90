!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/16              !
!===============================!

  program driver_openmp

  use m_1_type_definitions
  use omp_lib

  implicit none

  !  1. local parameters

  integer(kind=si) :: j
  integer(kind=si) :: k
 
  !  2. main parallel openmp code

  !$omp parallel      &
  !$omp default(none) &
  !$omp num_threads(2)

  !$omp do                 &
  !$omp ordered            &
  !$omp private(j)         &
  !$omp private(k)         &
  !$omp schedule(static,3)

  do k = 1, 3
    do j = 1, 2

    !$omp ordered
     
    write(*,*) omp_get_thread_num(), k, j

    !$omp end ordered
  
    end do
  end do

  !$omp end do

  !$omp end parallel
 
  end program driver_openmp

!======!
! FINI !
!======!
