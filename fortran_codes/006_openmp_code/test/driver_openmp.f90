!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/08/30              !
!===============================!

  program driver_openmp

#ifdef _OPENMP
  use omp_lib
#endif

  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=si), parameter :: K_DO_MAX = 100_si
  integer(kind=di), parameter :: DIMEN    = 4_di * 1000000_di
  integer(kind=si), parameter :: NUM_TASK = 4

  !  2. local variables  

  integer(kind=di) :: i
  integer(kind=si) :: j
  integer(kind=di) :: k
  integer(kind=si) :: count_sys_a
  integer(kind=si) :: count_sys_b
  integer(kind=si) :: count_rate
  integer(kind=si) :: count_max
  integer(kind=si) :: nt
  integer(kind=si) :: phys_cores
  real(kind=dr)    :: sum_loc
  
  !  3. build the matrices in parallel for K_DO_MAX times

  write(*,*)
  write(*,*) "  1 --> Parallel region starts"
  write(*,*) "  2 --> Opening many tasks"

  call system_clock(count_sys_a, count_rate, count_max)

  do k = 1, K_DO_MAX  ! benchmark starts here

    !$omp parallel      &
    !$omp default(none) &
    !$omp private(j)    &
    !$omp private(i)    &
    !$omp reduction(+:sum_loc)
 
    !$omp single

    do j = 1, NUM_TASK
      do i = 1, DIMEN
        !$omp task
        sum_loc = sum_loc + real(j+i,kind=dr)
        !$omp end task
      end do
    end do

    !$omp end single
    !$omp end parallel

  end do

  call system_clock(count_sys_b, count_rate, count_max)

  write(*,*) "  3 --> Real time used = ",                          &
             (count_sys_b - count_sys_a)/real(count_rate,kind=dr), &
             " seconds"

  !  4. final OpenMP step

  write(*,*) "  4 --> Loop steps are: ", K_DO_MAX
  write(*,*) "  5 --> Tasks opened: ", DIMEN*NUM_TASK

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)    &
  !$omp shared(phys_cores)
  
#ifdef _OPENMP
  nt = omp_get_num_threads()
  phys_cores = omp_get_num_procs()
#else
  nt = 1_si
  phys_cores = 1_si
#endif

  !$omp end parallel

  write(*,*) "  6 --> Number of OpenMP threads = ", nt
  write(*,*) "  7 --> Number of physical cores = ", phys_cores
  write(*,*) "  8 --> Exit", sum_loc 

  end program driver_openmp

!======!
! FINI !
!======!
