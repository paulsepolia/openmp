#!/bin/bash

  # 1. compile

  sh  001_compile_intel.sh
  sh  002_compile_gnu.sh
#  sh  003_compile_open.sh

  # 2. using 1, .., 4 openmp processes

  for var1 in 1 2 3 4
  do
    export OMP_NUM_THREADS=$var1
    sh  004_run_intel.sh
    sh  005_run_gnu.sh
#    sh  006_run_open.sh
  done

  # 3. exit
