!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/17              !
!===============================!

  program driver_openmp

  use omp_lib
  use m_1_type_definitions

  implicit none

  !  1. parameters

  integer(kind=si), parameter :: I_DO_MAX = 1000_si
  integer(kind=di), parameter :: J_DO_MAX = 1000000000_di
  logical, parameter          :: dyn_flag = .true.

  !  2. local variables

  integer(kind=si) :: nt
  integer(kind=si) :: i
  integer(kind=si) :: j
  real(kind=dr)    :: t_start_omp
  real(kind=dr)    :: t_end_omp
  real(kind=dr), dimension(:), allocatable :: k

  allocate(k(1:I_DO_MAX))

  !  3. setting OpenMP environment

  nt = 0_si

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)
  
  nt = omp_get_num_threads()
  call omp_set_dynamic(dyn_flag)

  !$omp end parallel

  !  4. main code execution

  t_start_omp = omp_get_wtime()

  write(*,*) ""

  !$omp parallel      &
  !$omp default(none) &
  !$omp private(i)    &
  !$omp private(j)    &
  !$omp shared(k)
   
  !$omp single

  do i = 1, I_DO_MAX

    !$omp task
    do j = 1, J_DO_MAX
      k(i) = k(i) + real(1_di, kind=dr)
    end do 
    !$omp end task

  end do

  !$omp end single  
   
  !$omp end parallel

  t_end_omp = omp_get_wtime()

  !  5. the outputs

  write(*,*) k(1)
  write(*,*) " 2 --> Real time used is (openmp) : ", (t_end_omp-t_start_omp)
  write(*,*) " 3 --> Number of threads are      : ", nt
  write(*,*) ""
 
  end program driver_openmp

!======!
! FINI !
!======!
