!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/04              !
!===============================!

!==========================!
! Calculates C=A*B,        !    
! where A is a m*k matrix, !
! B is a k*n matrix        !
! and C is a m*n matrix    !
!==========================!

  program driver_openmp 

  use omp_lib
  use m_1_type_definitions

  implicit none

  !  1. interface parameters

  integer(kind=si), parameter :: N = 2000
  integer(kind=si), parameter :: M = 2000
  integer(kind=si), parameter :: K = 2000

  !  2. local variables

  real(kind=dr), dimension(:,:), allocatable :: A
  real(kind=dr), dimension(:,:), allocatable :: B
  real(kind=dr), dimension(:,:), allocatable :: C
  real(kind=dr), dimension(:,:), allocatable :: CS
  real(kind=dr) :: alpha
  real(kind=dr) :: beta
  real(kind=dr) :: parallel_start
  real(kind=dr) :: parallel_end
  real(kind=dr) :: serial_start
  real(kind=dr) :: serial_end
  real(kind=dr) :: max_diff
  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: l

  !  3. allocate the matrices

  allocate(A(1:M,1:K))
  allocate(B(1:K,1:N))
  allocate(C(1:M,1:N))
  allocate(CS(1:M,1:N))

  !  4. initialize the matrices  

  call random_number(A)
  call random_number(B) 
  call random_number(C)
  
  CS = C

  !  5. initialize the constants

  call random_number(alpha)
  call random_number(beta)

  parallel_start = OMP_GET_WTIME()

  !  6. Start parellel region explicitly scoping all variables 

  !$omp parallel      &
  !$omp default(none) &
  !$omp private(i)    &
  !$omp private(j)    &
  !$omp private(l)    &
  !$omp shared(A)     &
  !$omp shared(B)     &
  !$omp shared(C)     &
  !$omp shared(alpha) &
  !$omp shared(beta) 

  !$omp do

  do i = 1, N
    do j = 1, M
      C(j,i) = beta*C(j,i) 
      do  l = 1, K
        C(j,i) = C(j,i) + alpha*A(j,l)*B(l,i) 
      end do
    end do
  end do

  !$omp end do

  !$omp end parallel

  parallel_end = OMP_GET_WTIME()

  !  7. serial execution

  serial_start = OMP_GET_WTIME()

  do i = 1, N
    do j = 1, M
      CS(j,i) = beta*CS(j,i) 
      do l = 1, K
        CS(j,i) = CS(j,i) + alpha*A(j,l)*B(l,i) 
      end do
    end do
  end do

  serial_end = OMP_GET_WTIME()

  !  8. compare the output to check 
  !      the parallel version is correct

  max_diff = maxval(abs(CS-C))

  write(*,*)
  write(*,*) " 1 --> Parallel version took : ", parallel_end - parallel_start, " seconds."       
  write(*,*) " 2 --> Serial version took   : ", serial_end - serial_start, " seconds."
  write(*,*) " 3 --> Maximum difference is : ", max_diff
  write(*,*) " 4 --> CS(M,N) = CS(M,N) + alpha*A(M,K)*B(K,N) "
  write(*,*) " 5 --> M = ", M
  write(*,*) " 6 --> N = ", N
  write(*,*) " 7 --> K = ", K
  write(*,*)

  !  9. Free up RAM

  deallocate(A)
  deallocate(B)
  deallocate(C)
  deallocate(CS)

  end program driver_openmp

!======!
! FINI !
!======!
