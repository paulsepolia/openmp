!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/17              !
!===============================!

  program driver_openmp

  use omp_lib
  use m_1_type_definitions
  use m_2_fibonacci_task

  implicit none

  !  1. parameters

  integer(kind=si), parameter :: n        = 44_si
  logical, parameter          :: dyn_flag = .true.


  !  2. local variables

  integer(kind=si) :: nt
  real(kind=dr)    :: t_start_omp
  real(kind=dr)    :: t_end_omp


  !  3. setting OpenMP environment

  nt = 0_si

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(nt)
  
  nt = omp_get_num_threads()
  call omp_set_dynamic(dyn_flag)

  !$omp end parallel


  !  4. main code execution

  t_start_omp = omp_get_wtime()

  write(*,*) ""

  !$omp parallel      &
  !$omp default(none)  
   
  !$omp single
    write(*,*)  " 1 --> fib(",  n, ") = ", fib(n)
  !$omp end single

  !$omp end parallel

  t_end_omp = omp_get_wtime()


  !  5. the outputs

  write(*,*) " 2 --> Real time used is (openmp) : ", (t_end_omp-t_start_omp)
  write(*,*) " 3 --> Number of threads are      : ", nt
  write(*,*) ""
 
  end program driver_openmp

!======!
! FINI !
!======!
