#!/bin/bash

# 1. compiling

  gfortran-4.8 -O3                      \
               -Wall                    \
               -std=f2008               \
               -fopenmp                 \
               -static                  \
               m_1_type_definitions.f90 \
               m_2_fibonacci_task.f90   \
               driver_openmp.f90        \
               -o x_gnu

# 2. cleaning 

  rm *.mod
