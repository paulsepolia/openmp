!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/09/17              !
!===============================!

  module m_2_fibonacci_task

  use m_1_type_definitions
  use omp_lib

  implicit none

  contains 
 
  !  1. fibonacci parallelized with TASK OpenMP workshare construct 

  recursive integer(kind=di) function fib(n) result(res)

  implicit none

  integer(kind=si) :: n
  integer(kind=di) :: i
  integer(kind=di) :: j

  !  2. main functions body

  if (n < 2_si) then
    ! step --> 1
    res = n
  else

    ! step --> 2

    !$omp task            &
    !$omp default(none)   &
    !$omp shared(i)       &
    !$omp firstprivate(n)  

      i = fib(n-1)

    !$omp end task

    ! step --> 3

    !$omp task            &
    !$omp default(none)   &
    !$omp shared(j)       &
    !$omp firstprivate(n)  

      j = fib(n-2)

    !$omp end task

    ! step --> 4

    !$omp taskwait
 
      res = i+j

  end if

  end function fib

  end module m_2_fibonacci_task

!======!
! FINI !
!======!
